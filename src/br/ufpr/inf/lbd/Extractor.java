package br.ufpr.inf.lbd;

import java.rmi.Remote;
import java.rmi.RemoteException;
import br.ufpr.inf.lbd.FeatureVector;


public interface Extractor extends Remote {
	public FeatureVector extractFeature(String jarPath) throws RemoteException;
}
