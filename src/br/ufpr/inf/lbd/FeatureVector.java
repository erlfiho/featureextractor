package br.ufpr.inf.lbd;

import java.io.Serializable;

public class FeatureVector implements Serializable {
	private static final long serialVersionUID = 1L;
	
	int cpu;
	  int mem;
	  int net;
	  int disk;

	  public FeatureVector () {
	    setCpu(0);
	    setDisk(0);
	    setMem(0);
	    setNet(0);
	  }
	  
	  public FeatureVector (int cpu, int disk, int mem, int net) {
		  setCpu(cpu);
		  setDisk(disk);
		  setMem(mem);
		  setNet(net);
	  }

	  public int getCpu() {
	    return cpu;
	  }

	  public void setCpu(int cpu) {
	    this.cpu = cpu;
	  }

	  public int getMem() {
	    return mem;
	  }

	  public void setMem(int mem) {
	    this.mem = mem;
	  }

	  public int getNet() {
	    return net;
	  }

	  public void setNet(int net) {
	    this.net = net;
	  }

	  public int getDisk() {
	    return disk;
	  }

	  public void setDisk(int disk) {
	    this.disk = disk;
	  }
}
