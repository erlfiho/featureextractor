package br.ufpr.inf.lbd;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmt.modisco.java.ClassFile;
import org.eclipse.gmt.modisco.java.Model;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.launching.LibraryLocation;
import org.eclipse.modisco.infra.discovery.core.exception.DiscoveryException;
import org.eclipse.modisco.java.discoverer.DiscoverJavaModelFromLibrary;


public class ExtractorImpl implements Extractor {
	public FeatureVector extractFeature(String jar) throws RemoteException {

		if (jar == null)
			return null;

		try {
			/* Open eclipse project */
			IWorkspace ws = ResourcesPlugin.getWorkspace();
			IProject project = ws.getRoot().getProject("FeatureExtractor");
			IProgressMonitor monitor = new NullProgressMonitor();

			/* Create a new project, in case there isn't one */
			if (!project.exists()) {
				System.out.println("Feature Extractor: Creating a new project.");
				IProjectDescription description = project.getDescription();
				description.setNatureIds(new String[] {"org.eclipse.jdt.core.javanature"});
				project.setDescription(description, monitor);
				project.create(monitor);
				if (!project.isOpen())
					project.open(monitor);	
			}
			System.out.println("Feature Extractor: Project location is " + project.getLocation());

			/* Create a link to the jar file, which is outside the project */
			IPath location = new Path(jar);
			IFile file = project.getFile(location.lastSegment());

			file.createLink(location, IResource.REPLACE, monitor);
			System.out.println("Feature Extractor: Creating link of file " + jar);
			System.out.println("Feature Extractor:                    to " + file.getFullPath());
			
			/* Create Java project, and 
			 * add location (incoming file) to the build path */
			IJavaProject javaProject = JavaCore.create(project);
			IPath filePath = project.getWorkspace().getRoot().getLocation().append(file.getFullPath());
			
			/* Set project fragment roots */
			Set<IClasspathEntry> e = new HashSet<IClasspathEntry>();
			e.addAll(Arrays.asList(javaProject.getRawClasspath()));
			
			boolean entryExist = false;
			for (IClasspathEntry c : javaProject.getRawClasspath()) {
				System.out.println("OSString: " + c.getPath().toOSString() + " location --> "+ filePath.toOSString());
				if ( filePath.toOSString().matches(c.getPath().toOSString()) ) {
					entryExist = true;
					break;
				}
			}
//			if (!entryExist) {
//				IClasspathEntry[] entries = javaProject.getRawClasspath();
//			    IClasspathEntry[] newEntries = new IClasspathEntry[entries.length + 1];
//			    System.arraycopy(entries, 0, newEntries, 0, entries.length);
//		    	newEntries[entries.length] = JavaCore.newContainerEntry(new Path(filePath.toOSString()));
//				javaProject.setRawClasspath(newEntries, monitor);
//			}
			
			IVMInstall vmInstall = JavaRuntime.getDefaultVMInstall();
			LibraryLocation[] locations= JavaRuntime.getLibraryLocations(vmInstall);
			for (LibraryLocation element : locations) {
				e.add(JavaCore.newLibraryEntry(element.getSystemLibraryPath(), null, null));
			}

			for (IPackageFragmentRoot r : javaProject.getPackageFragmentRoots()) {
				System.out.println("Feature Extractor: packageFragmentRoot: " + r.getPath().toOSString() + " location --> "+ filePath.toOSString());
				if ( filePath.toOSString().matches(r.getPath().toOSString()) ) {
					entryExist = true;
					break;
				}
			}	
			if (!entryExist) {
				e.add(JavaCore.newLibraryEntry(filePath, null, null));
			}
			javaProject.setRawClasspath(e.toArray(new IClasspathEntry[e.size()]), monitor);
			 
			for (IClasspathEntry c : javaProject.readRawClasspath())
				System.out.println("Feature Extractor: ++ classpath item = " + c.getPath());
			for (IPackageFragmentRoot r : javaProject.getPackageFragmentRoots())
				System.out.println("Feature Extractor: @@ Package fragment root is " + r.getPath());

			/* Create discover for jar file */
			DiscoverJavaModelFromLibrary discover = new DiscoverJavaModelFromLibrary();
			System.out.println("Feature Extractor: Location of file is " + file.getLocation());
			System.out.println("Feature Extractor: Full path of file is " + file.getFullPath());
			System.out.println("Feature Extractor: File path of file is " + filePath.toString());
			IPackageFragmentRoot root = JavaCore.createJarPackageFragmentRootFrom(file);
//			IPackageFragmentRoot root = javaProject.getPackageFragmentRoot(filePath.toString());
			if (root == null) {
				System.out.println("Feature Extractor: Error: Fragment root is null.");
				return null;		
			}

			/* Create model for jar file */
			discover.discoverElement(root, monitor);
			Resource resource = discover.getTargetModel();
			Model model = (Model) resource.getContents().get(0);

			/* Save Model XMI */
			File output = new File(filePath.toOSString().replace(".jar", ".xmi"));
			if (!output.exists())
				output.createNewFile();
			FileOutputStream outputStream = new FileOutputStream(output);
			resource.save(outputStream, null);
			
			/* DEBUG: list methods */
			System.out.println("Feature Extractor: Model name is " + model.getName());
			EList<ClassFile> classFiles = model.getClassFiles();
			for (ClassFile classFile : classFiles) {
				System.out.println("Feature Extractor: Class file name: " + classFile.getName());
			}

			System.out.println("Feature Extractor: Extraction finished.");
			return (new FeatureVector(0,0,0,0));
		} catch (CoreException e) {
			e.getMessage();
			e.printStackTrace();
		} catch (DiscoveryException e) {
			e.getMessage();
			e.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		return null;
	}
}
