package br.ufpr.inf.lbd;

import java.io.IOException;
import java.net.ServerSocket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class FeatureExtractor implements BundleActivator {
	private static String host = "localhost";
	private static String port = "45698";
	private static String bind_name = "FeatureExtractor";
	public  Extractor stub = null;
	public  Registry registry = null;
	public  ExtractorImpl server = null;

	public static boolean StopOldRMIServer() {
		/* Check if the RMI server port is being used */ 
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(Integer.parseInt(port));
			serverSocket.setReuseAddress(true);
			return true;
		} catch (IOException e) {
		} finally {
			if (serverSocket != null) {
				try {
					serverSocket.close();
				} catch (IOException e) {
					/* should not be thrown */
				}
			}
		}
		
		/* Close possible RMI server that is still alive */
		Registry registry;
		try {
			registry = LocateRegistry.getRegistry(Integer.parseInt(port));
			registry.unbind(bind_name);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		System.out.println("Starting Feature Extractor ... ");
//		if (System.getSecurityManager() == null)
//			System.setSecurityManager(new RMISecurityManager());
		

		try {
			/*
			String home = System.getenv("AUTOCONF_HOME");
			if (home == null) {
				System.out.println("AutoConf: error: AUTOCONF_HOME not set.");
				System.exit(-1);
			}

			Properties rmiconfig = new Properties();
			FileInputStream in = new FileInputStream(home + "/autoconf.conf");
			rmiconfig.load(in);
			String host = rmiconfig.getProperty("AUTOCONF_HOST");
			String port = rmiconfig.getProperty("AUTOCONF_PORT");
			
			*/

//			StopOldRMIServer();
			server = new ExtractorImpl();
			stub = (Extractor) UnicastRemoteObject.exportObject(server, 0);
			registry = LocateRegistry.createRegistry(Integer.parseInt(port));
			registry.rebind(bind_name, stub);			
			//in.close();

			System.out.println("Feature Extractor is running at " +  host + ":" + port);
		} catch (Exception e) {
			System.err.println("Feature Extractor: error: " + e.getMessage());			
			e. printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		if (registry == null) {
			System.out.println("Feature Extractor: Cannot stop RMI registry once it has not been started.");
		} else {
			// registry = LocateRegistry.getRegistry(Integer.parseInt(port));
			registry.unbind(bind_name);	
			UnicastRemoteObject.unexportObject(registry, true);
			System.out.println("Feature Extractor has been stoped.");
		}
	}
}
